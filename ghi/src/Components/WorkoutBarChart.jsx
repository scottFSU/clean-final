import { BarChart, Card, Title, Subtitle } from "@tremor/react"


function WorkoutBarChart({ chart }) {
    return (
        <Card>
            <Title className="text-orange-600">Your Most Recent Murph</Title>
            <Subtitle></Subtitle>
            <BarChart
                className="mt-6"
                showLegend={true}
                noDataText="This graph is for closers."
                rotateLabelX={{
                    angle: 270,
                    verticalShift: 20,
                    xAxisHeight: 45,
                }}
                data={chart}
                index="name"
                categories={["Seconds"]}
                colors={["orange-600"]}
                yAxisWidth={40}
            />
        </Card>
    )
}

export default WorkoutBarChart
