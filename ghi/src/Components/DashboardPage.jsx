import { useEffect, useState } from "react"
import { Navigate } from "react-router-dom"
import { API_HOST } from "../main"
import fetchData from "../Common/fetchData"
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react"
import formatUserTime from "../Common/formatUserTime"
import WorkoutBarChart from "./WorkoutBarChart"
import WorkoutLineChart from "./WorkoutLineChart"
import getDuration from "../Common/duration"


function DashboardPage() {
    const [userStats, setUserStats] = useState({})
    const [workouts, setWorkouts] = useState([])
    const [chart, setChart] = useState([])
    const [lineChart, setLineChart] = useState([])
    const { token } = useAuthContext()


    useEffect(() => {
        const tokenURL = `${API_HOST}/token`
        const workoutsURL = `${API_HOST}/api/workouts/mine`
        const fetchConfig = {
            credentials: "include",
        }
        fetchData(tokenURL, fetchConfig, setUserStats)
        fetchData(workoutsURL, fetchConfig, setWorkouts)
    }, [])

    useEffect(() => {
        if (workouts[0]) {
            const recentChartData = [
                {
                    name: "Run 1",
                    Seconds: workouts[0].run_1 || 0
                },
                {
                    name: "Set 1",
                    Seconds: workouts[0].set_1,
                },
                {
                    name: "Set 2",
                    Seconds: workouts[0].set_2,
                },
                {
                    name: "Set 3",
                    Seconds: workouts[0].set_3,
                },
                {
                    name: "Set 4",
                    Seconds: workouts[0].set_4,
                },
                {
                    name: "Set 5",
                    Seconds: workouts[0].set_5,
                },
                {
                    name: "Set 6",
                    Seconds: workouts[0].set_6,
                },
                {
                    name: "Set 7",
                    Seconds: workouts[0].set_7,
                },
                {
                    name: "Set 8",
                    Seconds: workouts[0].set_8,
                },
                {
                    name: "Set 9",
                    Seconds: workouts[0].set_9,
                },
                {
                    name: "Set 10",
                    Seconds: workouts[0].set_10,
                },
                {
                    name: "Run 2",
                    Seconds: workouts[0].run_2,
                },
            ]
            setChart(recentChartData)
        }
    }, [workouts])

    useEffect(() => {
        const last10Workouts = workouts.slice(0, 10)
        const lineChartData = last10Workouts
            .filter((workout) => workout.is_completed)
            .map((workout) => {
                const workoutChartData = {
                    name: workout.date,
                    Seconds: getDuration(workout),
                }
                return workoutChartData
            })
        setLineChart(lineChartData)
    }, [workouts])

    if (!token) {
        return <Navigate to="/login" />
    }

    return (
        <div className="container mx-auto p-4">
            <div className="justify-center text-center">
                <div className="white-text-box px-2 py-1 inline-block">
                    <h1>
                        {" "}
                        {userStats.user
                            ? `Hello ${userStats.user.username}`
                            : "Bad username"}{" "}
                    </h1>
                </div>
            </div>
            <div className="flex mt-4">
                <div className="flex">
                    <div className="flex-1 bg-white standard-div space-y-4 bg-white shadow-lg rounded-lg p-5 m-4">
                        <h1>Stats</h1>
                        {workouts[0] ?
                        <table className="w-auto bg-gradient-to-t from-custom-orange to-orange-400 border border-black justify-center text-center">
                            <thead>
                                <tr>
                                    <th className="table-margin-padding">
                                        Date
                                    </th>
                                    <th className="table-margin-padding">
                                        Time
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {workouts.map((workout, index) => {
                                    const duration =
                                        workout.run_1 +
                                        workout.set_1 +
                                        workout.set_2 +
                                        workout.set_3 +
                                        workout.set_4 +
                                        workout.set_5 +
                                        workout.set_6 +
                                        workout.set_7 +
                                        workout.set_8 +
                                        workout.set_9 +
                                        workout.set_10 +
                                        workout.run_2
                                    return (
                                        <tr
                                            key={workout.id}
                                            className={
                                                index % 2 === 0
                                                    ? "bg-gray-200 border border-black"
                                                    : "border border-black"
                                            }
                                        >
                                            <td
                                                className={
                                                    "table-margin-padding"
                                                }
                                            >
                                                {workout.date}
                                            </td>
                                            <td
                                                className={
                                                    "table-margin-padding"
                                                }
                                            >
                                                {formatUserTime(duration)}
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table> :
                        <div>No data yet</div>
                        }
                    </div>
                </div>
                <div className="flex-1">
                    <div className="flex flex-col space-y-4 bg-black shadow-lg rounded-lg p-5 m-4">
                        {workouts ? (
                            <div className="grid grid-cols-2 gap-4">
                                <div>
                                    <WorkoutBarChart chart={chart} />
                                </div>
                                <div>
                                    <WorkoutLineChart lineChart={lineChart} />
                                </div>
                            </div>
                        ) : (
                            <div className="text-white text-center">
                                No Data Yet
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DashboardPage
