import { useState, useEffect } from 'react'
import {useNavigate, Link, Navigate} from 'react-router-dom'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react';


function Countdown() {
    const [time, setTime] = useState(11);
    const navigate = useNavigate();
    const { token } = useAuthContext()

    useEffect(() => {
        const redirectTimer = setTimeout(() => {
        navigate('/workout');
        }, 11000);
        return () => clearTimeout(redirectTimer);
    }, [navigate])

    useEffect(() => {
        const timer = setInterval(() => {
        if (time > 1)
        setTime((prevTime) => prevTime - 1)
        }, 1000)
        return () => {
            clearInterval(timer);
        }
    }, [time]);

    if (!token) {
        return <Navigate to='/login' />
    }

    return (
        <div className="flex flex-col items-center justify-center h-screen">
            <h3 className="mb-4">Your workout begins in...</h3>
            <h1 className="flex items-center justify-center text-9xl text-green-600 text-growth">{time > 1 ? (time - 1)  : "GO!"}</h1>
            <button className="bg-green-600 text-black flex items-center text 2xl mt-4 hover:bg-white font-bold py-2 px-4 rounded">
                <Link to="/workout">Bypass and Begin!</Link>
            </button>
            <button className="bg-red-500 text-black flex items-center text 2xl mt-4 hover:bg-white font-bold py-2 px-4 rounded">
                <Link to="/dashboard">ABORT WORKOUT</Link>
            </button>
        </div>
    )
}

export default Countdown
