import { API_HOST } from "../main"
import useSWR from "swr"


const fetcher = async (...args) => {
    const fetchConfig = {
        credentials: "include"
    }
    const response = await fetch(...args, fetchConfig)
    if (response.ok) {
        const data = await response.json()
        if (data) {
            return data;
        }
    }
    else {
        console.log("error")
    }
}


const useUser = () => {
    const { data, error, isLoading } = useSWR(`${API_HOST}/token`, fetcher)
    return {
        user: data, isError: error, isLoading
    }
}

export default useUser
