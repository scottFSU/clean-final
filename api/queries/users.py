from fastapi import HTTPException, status
from models.models import (
    DuplicateUserError,
    Error,
    UserIn,
    UserOutWithHashedPassword,
)
from queries.pool import pool


class UsersQueries:
    def create(
        self, user: UserIn, hashed_password: str
    ) -> UserOutWithHashedPassword | Error:
        username_exists = self.get(user.username)
        if isinstance(username_exists, UserOutWithHashedPassword):
            raise DuplicateUserError(
                "Cannot create user from provided inputs."
            )
        # connect to the database
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO users
                    (username, password)
                    VALUES
                        (%s, %s)
                    RETURNING id, username, password;
                    """,
                    [user.username, hashed_password],
                )
                new_user = result.fetchone()
                if not new_user:
                    return Error(message="Could not create user.")
                id = new_user[0]
                username = new_user[1]
                hashed_password = new_user[2]
                return UserOutWithHashedPassword(
                    id=id, username=username, hashed_password=hashed_password
                )

    def get(self, username: str) -> UserOutWithHashedPassword | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        username,
                        password
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if not record:
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Could not find a user with this username.",
                        )
                    id = record[0]
                    username = record[1]
                    hashed_password = record[2]
                    return UserOutWithHashedPassword(
                        id=id,
                        username=username,
                        hashed_password=hashed_password,
                    )
        except Exception:
            return None
