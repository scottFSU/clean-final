steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE IF NOT EXISTS workouts (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INTEGER NOT NULL REFERENCES users(id),
            date DATE,
            run_1 INTEGER,
            set_1 INTEGER,
            set_2 INTEGER,
            set_3 INTEGER,
            set_4 INTEGER,
            set_5 INTEGER,
            set_6 INTEGER,
            set_7 INTEGER,
            set_8 INTEGER,
            set_9 INTEGER,
            set_10 INTEGER,
            run_2 INTEGER,
            is_completed BOOL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE IF EXISTS workouts;
        """,
    ]
]
