from pydantic import BaseModel
from datetime import date
from jwtdown_fastapi.authentication import Token


class DuplicateUserError(ValueError):
    pass


class Error(BaseModel):
    message: str


class UserIn(BaseModel):
    username: str
    password: str
    verified_password: str


class UserOut(BaseModel):
    id: int
    username: str


class UserOutWithHashedPassword(UserOut):
    hashed_password: str


class WorkoutForm(BaseModel):
    date: date
    run_1: int | None
    set_1: int | None
    set_2: int | None
    set_3: int | None
    set_4: int | None
    set_5: int | None
    set_6: int | None
    set_7: int | None
    set_8: int | None
    set_9: int | None
    set_10: int | None
    run_2: int | None

    def check_completion(self):
        return True if (
            self.run_1 and
            self.set_1 and
            self.set_2 and
            self.set_3 and
            self.set_4 and
            self.set_5 and
            self.set_6 and
            self.set_7 and
            self.set_8 and
            self.set_9 and
            self.set_10 and
            self.run_2
            ) else False


class WorkoutIn(WorkoutForm):
    user_id: int
    is_completed: bool


class WorkoutOut(WorkoutIn):
    id: int


class LeaderboardOut(WorkoutOut):
    duration: int
    username: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str
