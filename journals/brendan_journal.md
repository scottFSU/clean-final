# Daily Journal

## 7 Feb 2024
Feature complete! We made it. Today was a good day, we knocked out the rest of error handling (Scott and I did sign-up issues), and then we implemented Tremor charts in the dashboard.

My contributions today:
1. I rebuilt our sign-up flow so that we don't rely on galvanize-jwtdown-for-react's `register` method. This enabled us to build in the error handling we wanted to, and also to get rid of the really weird dynamic where signing up with an existing username and the password associated with that user would not create a duplicate account but _would log that attempt in_!. How silly :'). We fixed that.
2. After Scott was done with the first implementation of Tremor, I refactored one of the pieces of logic (creating the data structure for the progress chart) so that we could gracefully handle any number of workouts.

## 5 Feb 2024
Slow and steady, smooth progress. We did a lot of error handling today. I got nerd-sniped trying to figure out how jwtdown_fastapi works, but then that turned out to be useful when our error handling went awry and we needed to dig into the innards of... jwtdown_fastapi :)

My contributions today:
1. I finalized the error handling for logging in with incorrect username / incorrect password, and attempting to sign-up with an existing username.
2. Helped to guide folks through their features.

## 31 Jan 2024
Today was a major cleanup day. Folks added a lot of CSS, we added redirection to many pages, we made sure all of our buttons worked and had the intended behavior, etc. We also finally landed some important functionality (though in a bit of a hacky manner): redirect on login/signup.

My contributions today:
1. Added the ability to abandon a workout in the CreateWorkout component. The first button press opens a modal, with options to "go back" or "confirm abandon".
2. Updated the dashboard so that the table of workouts displays differently depending on whether the workout was completed or not.
3. A whole bunch of minor fixes / improvements.

## 30 Jan 2024
Today was a big day for us (and for me personally). We got through some of the biggest remaining hurdles, and we feel like the path is clear for us to hammer out the remaining chunks of our MVP.

My contributions today:
1. I re-wrote our CreateWorkout component, fixing the post query, the interval timer, the formatting of the time display, and a bunch of other little tweaks to make that page look nice and work well.
2. I helped figure out what was going on with the redirection-of-non-logged-in users. We want to lock down most of our pages to only be displayed to logged-in users. Figuring how to implement that block was a bit tricky. Ran into some race conditions. But we got through it.

# 29 Jan 2024
My contributions today:
1. Fixed some issues with the Leaderboard component. I corrected the query and helped implement the code to issue the POST-request to the backend.
2. Started work on the CreateWorkout component, building the navigation and a bunch of the React structure.

## 23 Jan 2024
Got some feedback from Riley on our backend implementation, which forced us to reconsider some of our code. It was good feedback, I'm glad we spoke to him. Helped us to simplify a number of aspects of our system, as well as improve how certain components work. For instance, we were forcing the frontend to provide two "aggregate values" `total_duration` and `is_completed`. Both of those could be calculated from other fields, so we removed them from the frontend and implemented logic for those on the backend. We completely removed `total_duration` from our workouts table. We calculate this dynamically in our SQL query where necessary. And while we do store `is_completed` in the workouts table, we implemented a basic method on one of our pydantic model classes to make this easy to prepare on the backend. Very productive day.

My contributions:
1. I re-did our implementation of the Leaderboard API endpoint. The main trick here was to create the synthetic `total_duration` column in the SQL query.
2. I helped triage a few bugs with modifying how we were pulling `user_id` in a number of router functions.

## 22 Jan 2024
Smooth sailing today. Team is humming. We finished up our API happy paths, and have decided to leave error handling for after we learn unit tests on Friday. Next up is the frontend. We'll wire up all of the React components, make sure they return data, implement auth, and then add CSS styling.

My contributions today:
1. Coded the `create-workout` API endpoint. Felt good. Natural. I didn't have the pattern memorized, but it came easily, and the squad filled in all the gaps.
2. Lots of debugging. Figured out the path collision issue between `api/workouts/{workout_id}` and `api/workouts/mine`. Helped Scott with Get Leaderboard, Richard with his Get Single Workout, and Adriel with Get All User Workouts.

## 18 Jan 2024
Pretty good day today. We got through database setup pretty smoothly. Then stubbed out all of our api endpoints, and created most of the pydantic models we'll need for tackling auth. We ultimately didn't get to start on auth today, but we made steady progress, and I think we'll put a big dent into auth tomorrow.

Some of my main contributions today:
1. Fixed a bunch of issues with our docker-compose file so that our fastapi service would actually await the db.
2. Helped troubleshoot issues with the creation of our migrations tables.
3. Stubbed out our first api endpoint, so I created a bunch of the directory structure, imports, etc.

## 17 Jan 2024
Today was a pretty tough day, to be honest. We had a great standup, aligning on a plan for the day that seemed ambitious but which I had confidence that we would put a dent in. But in one of our "warmup" tasks, we ran into a gnarly bug with psycopg that ended up taking >5 hours to resolve.

So what did I learn today?
1. Yet again, I learn the lesson: be iterative. We shouldn't have changed our usernames and passwords AND done the `.env` file config at the same time. We should have set up the `.env` file with the existing credentials first, and only then changed things. Make a tiny change, then test. Make a tiny change, then test. If we had done this, it's _possible_ that we would have figured out sooner that the issue had to do with our password.

2. Obviously, I learned that I need to be careful with postgres database passwords. No @s allowed.

Tomorrow's another day. Hopefully we'll tackle auth. I'm excited to take that on :)
